const array = require('./3-arrays-vitamins.cjs')

const itemsWithVitaminC = array.filter(item => {
    let vitamin = item.contains.split(",").map(vitamin => vitamin.trim())

    return vitamin.includes('Vitamin C')
});

console.log(itemsWithVitaminC);
