const array = require('./3-arrays-vitamins.cjs')

const sortItems = array.sort((a, b) => {
    const aVitamins = a.contains.split(', ');
    const bVitamins = b.contains.split(', ');
    return aVitamins.length - bVitamins.length
  });
  
console.log(sortItems);