const array = require('./3-arrays-vitamins.cjs')

const itemsWithVitaminC = array.filter(item => {
    let vitamin = item.contains.split(",").map(eachVitamin => eachVitamin.trim())

    return vitamin.includes('Vitamin A')
});

console.log(itemsWithVitaminC);
