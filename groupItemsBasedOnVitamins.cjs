const array = require('./3-arrays-vitamins.cjs')

const groupItems = array.reduce((acc, item) => {
    const vitamins = item.contains.split(', ');
    
    vitamins.forEach(vitamin => {
      if (acc[vitamin]) {
        acc[vitamin].push(item.name);
      } else {
        acc[vitamin] = [item.name];
      }
    });
    return acc;
  }, {});
  
  console.log(groupItems);